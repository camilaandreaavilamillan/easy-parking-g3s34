# crud clientes con sqlAlchemy
# date 07-10-2021, este Crud, es el Bus.py que hizo el profe
from os import stat
#from werkzeug.datastructures import V
from server import database
from datetime import datetime

class Clientes(database.Model):
    __tablename__='clientes'
    #CREATE TABLE public.clientes (
        #    idcliente integer NOT NULL,
        #    nitcliente character(20) NOT NULL,
        #    nombres character varying(40),
        #    apellidos character varying(40),
        #    telefono character varying(30),
        #    email character varying(70),
        #    fecharegistro date,
        #    sexo character(1),
        #    activo character(1)
    #);
    idcliente = database.Column(database.Integer,primary_key=True)
    nitcliente = database.Column(database.Integer, nullable=False)
    nombres = database.Column(database.Integer, nullable=False)
    apellidos = database.Column(database.String, nullable=False)
    telefono = database.Column(database.String, nullable=False)
    email = database.Column(database.String, nullable=False)
    fecharegistro = database.Column(database.String(datetime), nullable=False, default=datetime.now())
    sexo = database.Column(database.String, nullable=False)
    activo = database.Column(database.String, nullable=False, default='S')

    def __init__(self, nitcliente, nombres, apellidos, telefono, email, fecharegistro, sexo, activo ):
        self.nitcliente=nitcliente
        self.nombres=nombres
        self.apellidos=apellidos
        self.telefono=telefono
        self.email=email
        self.fecharegistro=fecharegistro
        self.sexo=sexo
        self.activo=activo

    def create(self):
        database.session.add(self)
        database.session.commit()

    #delete cliente
    @staticmethod
    def delete(id_delete):
        Clientes.query.filter_by(id=id_delete).delete()
        database.session.commit()

    #update cliente
    @staticmethod 
    def update_cliente(id_update,up_nitcli, up_nombre, up_apel, up_tel, up_email, up_fechareg, up_sexo, up_activo):
        cliente = Clientes.query.get(id_update)
        cliente.nitcliente = up_nitcli
        cliente.nombres = up_nombre
        cliente.apellidos = up_apel
        cliente.telefono = up_tel
        cliente.email = up_email
        cliente.fecharegistro = up_fechareg
        cliente.sexo = up_sexo
        cliente.activo = up_activo
        database.session.commit()
    
    # select toda la base de datos clientes
    @staticmethod
    def get_all():
        return Clientes.query.all() # select 
        #return Clientes.query.order.by(Clientes.apellidos.asc()).all() # select * from clientes oder by idcliente ASC;
    
    #buscando id cliente
    @staticmethod
    def get_idcliente(id_to_find):
        return Clientes.query.filter_by(idcliente=id_to_find)
    
    #select por busqueda de un campo dado ej. nombres
    #busca un nombre en la base de datos
    @staticmethod  
    def get_name(nombre):
        return Clientes.query.filter_by(nombres=nombre)
    
    #buscando un nit de cliente
    @staticmethod
    def get_nit(nit_to_find):
        return Clientes.query.filter_by(nitcliente=nit_to_find)
    
    #ordenado por apellidos
    @staticmethod
    def get_order_by_apellido_cli():
        return Clientes.query.order_by(Clientes.apellidos)
    
# crud usuarios con sqlAlchemy
# date 11-10-2021, este Crud, es el Bus.py que hizo el profe
from os import curdir, stat
from server import database
from datetime import datetime

class Usuarios(database.Model):
    __tablename__='usuarios'
    # CONTINUA CRUD DE TABLA USUARIOS - 4
        #CREATE TABLE public.usuarios (
            #idusuario integer NOT NULL,
            #identificacion character varying(20) NOT NULL,
            #nombres character varying(50),
            #direccion character varying(60),
            #telefono character varying(30),
            #email character varying(70),
            #clave character varying(40),
            #ultimaclave character varying(40),
            #fechaactualiza date,
            #sexo character(1),
            #crudinsertar character(1),
            #crudconsultar character(1),
            #crudeliminar character(1),
            #crudactualizar character(1),
            #activo character(1),
            #username character(30)
    #);
    
    idusuario = database.Column(database.Integer,primary_key=True)
    identificacion = database.Column(database.String, nullable=False)
    nombres = database.Column(database.string, nullable=False)
    direccion = database.Column(database.String, nullable=False)
    telefono = database.Column(database.String, nullable=False)
    email= database.Column(database.String, nullable=False)
    clave = database.Column(database.String, nullable=False)
    ultimaclave = database.Column(database.String, nullable=False)
    fechaactualiza = database.Column(database.String(datetime), nullable=False, default=datetime.now())
    sexo = database.Column(database.String, nullable=False)
    crudinsertar = database.Column(database.String, nullable=False)
    crudconsultar = database.Column(database.String, nullable=False)
    crudeliminar = database.Column(database.String, nullable=False)
    crudactualizar = database.Column(database.String, nullable=False)
    activo= database.Column(database.String, nullable=False,default='S')
    username = database.Column(database.String, nullable=False)
    
    def __init__(self, identificacion, nombres, direccion, telefono, email, clave,ultimaclave,fechaactualiza, sexo, crudinsertar,crudconsultar,crudeliminar, crudactualizar, activo, username ):
        self.identificacion=identificacion
        self.nombres=nombres
        self.direccion=direccion
        self.telefono=telefono
        self.email=email
        self.clave=clave
        self.ultimaclave=ultimaclave
        self.fechaactualiza=fechaactualiza
        self.sexo=sexo
        self.crudinsertar=crudinsertar
        self.crudconsultar=crudconsultar
        self.crudeliminar=crudeliminar
        self.crudactualizar=crudactualizar
        self.activo=activo
        self.username=username
            
    def create(self):
            database.session.add(self)
            database.session.commit()

    #delete usuarios
    @staticmethod
    def delete(id_usuario):
        Usuarios.query.filter_by(id=id_usuario).delete()
        database.session.commit()

    #update usuarios
    @staticmethod 
    def update_usuario(id_update,up_ident, up_nombre, up_dir, up_tel, up_email, up_clave, up_ultclave, up_fechaact, up_sexo, up_crudins, up_crudcons, up_crudeli,up_crudact,up_activo, up_user):
        usuario = Usuarios.query.get(id_update)
        usuario.identificacion = up_ident
        usuario.nombres=up_nombre
        usuario.direccion=up_dir
        usuario.telefono=up_tel
        usuario.email=up_email
        usuario.clave=up_clave
        usuario.ultimaclave=up_ultclave
        usuario.fechaactualiza=up_fechaact
        usuario.sexo=up_sexo
        usuario.crudinsertar=up_crudins
        usuario.crudconsultar=up_crudcons
        usuario.crudeliminar=up_crudeli
        usuario.crudactualizar=up_crudact
        usuario.activo=up_activo
        usuario.username=up_user
        database.session.commit()

# select toda la base de datos usuarios
    @staticmethod
    def get_all():
        return Usuarios.query.all() # select * from usuarios
    
    #buscando id cliente
    @staticmethod
    def get_idusuario(id_to_find):
        return Usuarios.query.filter_by(idusuario=id_to_find)
    
    #select por busqueda de un campo dado ej. nombres
    #busca un nombre en la base de datos
    @staticmethod  
    def get_name(nombre):
        return Usuarios.query.filter_by(nombres=nombre)
    
    #buscando una identificacion
    @staticmethod
    def get_ident(ident_to_find):
        return Usuarios.query.filter_by(identificacion=ident_to_find)
    
    #ordenado por nombres
    @staticmethod
    def get_order_by_nombres():
        return Usuarios.query.order_by(Usuarios.nombres)   
    
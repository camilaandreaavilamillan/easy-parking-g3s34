from Crud_g3s34 import Crud2
from utilities import *
from flask import Flask, request,make_response,redirect,render_template
from flask.wrappers import Response
#import json 
app = Flask(__name__) #instancia de flask

@app.route('/')
def index():
    # CONECTAR LA BASE DE DATOS, EN HEROKU
    # conexion de base de datos en heroku. 24-09-2021
    crud2 = open_database_heroku()
    clientes = crud2.leer_clientes() 
    #clientes es una lista de tuplas, cada registro es una tupla
    #crud2.close()
    lista_clientes=[] 
    for cliente in clientes:#cliente es la tupla de cada registro 
        lista_clientes.append({"idcliente":cliente[0],"nitcliente": cliente[1],
        "nombres": cliente[2], "apellidos":cliente[3],
        "telefono": cliente[4], "email":cliente[5],
        "fecharegistro": cliente[6], "sexo":cliente[7],
        "activo": cliente[8]})
    #respuesta = json.dumps(lista_clientes)  #NO FUNCIONA json
    #print(lista_clientes)
    crud2.close()#cerrando la conexion 
    return render_template("index.html",clientes=lista_clientes )

#CREATE TABLE public.clientes (
    #    nitcliente character(20) NOT NULL,
    #    nombres character varying(40),
    #    apellidos character varying(40),
    #    telefono character varying(30),
    #    email character varying(70),
    #    fecharegistro date,
    #    sexo character(1),
    #    activo character(1)
    #);

@app.route('/login',methods=['GET','POST'])
def login():
    if(request.method== 'GET'):
        return render_template('login.html')
    if(request.method=='POST'):
        nombreUsuario = request.form.get("username")
        passwd = request.form.get("passwd")
        print("nombre de usuario= "+nombreUsuario+" passwd="+passwd)
        return render_template('login.html')

@app.route("/show_edit_cliente_form",methods=['GET','POST'])
def show_edit_cliente_form():
    if(request.method=='POST'):
        id_cliente=request.form.get("id_cli_edit")
        #print (id_cliente)
        print("el id del cliente es: show_edit_cliente_form ",id_cliente)
        crud2 = open_database_heroku()
        # #clientes es una lista de una tupla
        clientes = crud2.leer_cliente(id_cliente) 
        crud2.close()
        cliente = clientes[0]
        print(cliente)
        dic_cliente = {"idcliente":cliente[0],"nitcliente":cliente[1],"nombres":cliente[2],"apellidos":cliente[3],"telefono":cliente[4],"email":cliente[5],"fecharegistro":cliente[6],"sexo":cliente[7],"activo":cliente[8]}    
        return render_template("edit_cliente.html",cliente=dic_cliente)
                           
@app.route("/delete_cliente",methods=['GET','POST'])
def delete_cliente():
    if(request.method=='POST'):
        id=request.form.get("id_cli_del")
        cr = open_database_heroku()
        cr.eliminar_cliente(id)
        cr.close()
        response= make_response(redirect('/get_clientes'))
        return response

@app.route("/form_add_cliente",methods=['GET','POST'])
def form_add_cliente():
    return render_template('add_cliente.html')

@app.route("/add_cliente",methods=['GET','POST'])
def add_cliente():
    if(request.method=='POST'):
        nitcliente=request.form.get("nitcliente")
        nombres=request.form.get("nombres")
        apellidos=request.form.get("apellidos")
        telefono=request.form.get("telefono")
        email=request.form.get("email")
        fecharegistro=request.form.get("fecharegistro")
        sexo=request.form.get("sexo")
        activo=request.form.get("activo")
        cr = open_database_heroku()
        cr.insertar_cliente(nitcliente,nombres, apellidos, telefono, email, fecharegistro, sexo, activo)
        cr.close()
        response= make_response(redirect('/get_clientes'))
        return response

@app.route("/edit_cliente",methods=['GET','POST'])
def edit_cliente():
    if(request.method=='POST'):
        idcliente=request.form.get("idcliente")
        nitcliente=request.form.get("nitcliente")
        nombres=request.form.get("nombres")
        apellidos=request.form.get("apellidos")
        telefono=request.form.get("telefono")
        email=request.form.get("email")
        fecharegistro=request.form.get("fecharegistro")
        sexo=request.form.get("sexo")
        activo=request.form.get("activo")
        print("**idcliente=",idcliente," nombres=",nombres, " apellidos=",apellidos)
        crud2 = open_database_heroku()
        crud2.update_cliente(idcliente,nitcliente,nombres, apellidos, telefono, email, fecharegistro, sexo, activo)
        crud2.close()
        response= make_response(redirect('/get_clientes'))
        return response
    if(request.method=='GET'):
        print("El metodo es GET en edit_cliente")
        
<<<<<<< HEAD
=======

>>>>>>> 6faac85bbd9a39efa8ace0f3ab720dc24853ba08
@app.route("/get_clientes")
def get_clientes():
    crud2 = open_database_heroku()
    clientes = crud2.leer_clientes() #clientes es una lista de tuplas, cada registro es una tupla
    lista_clientes=[]
    for cliente in clientes: #cliente es la tupla de cada registro 
        lista_clientes.append({"idcliente":cliente[0],"nitcliente": cliente[1],
     "nombres": cliente[2], "apellidos":cliente[3],
     "telefono": cliente[4], "email":cliente[5],"fecharegistro":cliente[6],
     "sexo":cliente[7],"activo":cliente[8]})
<<<<<<< HEAD
    crud2.close()    
=======
>>>>>>> 6faac85bbd9a39efa8ace0f3ab720dc24853ba08
    #respuesta = json.dumps(lista_clientes)
    return render_template("mostrar_clientes.html",clientes=lista_clientes)

@app.route("/add_usuario")
def registro():
    return render_template('add_usuario.html')


@app.route("/test")
def test():
    crud2 = open_database_heroku()
    clientes = crud2.leer_clientes() #clientes es una lista de tuplas, cada registro es una tupla
    print(clientes)
    return "ok-revisar resultado en consola"

if __name__=="__main__":
    #punto de partida de ejecucion del programa    
    print("arrancando servidor...")
    app.run(debug=True,host="0.0.0.0")
    #app.run(debug=False)
    
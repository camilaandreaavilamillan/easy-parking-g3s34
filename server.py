#server.py proyecto parqueadero
from flask import Flask, request,make_response,redirect,render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__) #instancia de flask
    #postgresql://<nombre_usuario>:<password>@<host>:<puerto>/<nombre_basededatos>
    #aqui tambien se puede colocar los parametros de heroku-base de datos en la nube
    # app.config['SQLALCHEMY_DATABASE_URI']= 'postgresql://postgres:caam1992@localhost:5432/transportes'    #esta es una conexion local
    #crud2 = Crud2(host, database, user, passwd) #esta es la conexion de crud, flask
    #sqlALChemy user, passwd, host, puerto, basedadatos  
    #sqlALChemy 'postgresql://user:passwd@host:puerto/basedadatos' va database_uri
app.config['SQLALCHEMY_DATABASE_URI']= 'postgresql://xnokvkpmamheih:eef8f73f5c42684fdcb40d1a8388745c812d19a956762027653dead3e5907611@ec2-35-171-171-27.compute-1.amazonaws.com:5432/ddiu8vd56t7vo8'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(32)
database =SQLAlchemy(app)
from models.Clientes import * 
from models.Usuarios import *


@app.route('/')
def index():
    clientes=Clientes.get_all()
    for client in clientes:
        print("idcliente :"+str(client.idcliente)," nombres :",client.nombres, " fecha :"+str(client.fecharegistro))
    return (render_template("index.html"))

# ROUTES para usuarios
@app.route("/get_usuarios")
def get_usuarios():
    return render_template("mostrar_usuarios.html",users=Usuarios.get_all())
    
@app.route("/form_add_usuario",methods=['GET','POST'])
def form_add_usuario():
    return render_template('add_usuario.html')

@app.route("/add_usuario",methods=['GET','POST'])
def add_usuario(): 
    #id,identificacion,nombres,direccion,telefono,email,clave,#ultimaclave,#fechaactualiza,sexo,crudinsertar,crudconsultar,#crudeliminar,#crudactualizar,activo,username
    if (request.method=='POST'):
        identificacion=request.form.get("identificacion")
        nombres=request.form.get("nombres")
        direccion=request.form.get("direccion")
        telefono=request.form.get("telefono")
        email=request.form.get("email")
        clave=request.form.get("clave")
        ultimaclave=request.form.get("ultimaclave")
        fechaactualiza=request.form.get("fechaactualiza")
        sexo=request.form.get("sexo")
        crudinsertar=request.form.get("crudinsertar")
        crudconsultar=request.form.get("crudconsultar")
        crudeliminar=request.form.get("crudeliminar")
        crudactualizar=request.form.get("crudactualizar")
        activo=request.form.get("activo")
        username=request.form.get("username")
        usuario_to_add= Usuarios(identificacion,nombres,direccion,telefono,email,clave,ultimaclave,fechaactualiza,sexo,crudinsertar,crudconsultar,crudeliminar,crudactualizar,activo,username)
        usuario_to_add.create()
        response= make_response(redirect('/get_usuarios'))
        return response
    
from models.routesClientes import *

# ROUTES para clientes 
@app.route("/get_clientes")
def get_clientes():
    return render_template("mostrar_clientes.html",clientes=Clientes.get_order_by_apellido())
    #return render_template("mostrar_clientes.html",clientes=Clientes.get_all())

@app.route("/show_edit_cliente_form",methods=['GET','POST'])
def show_edit_cliente_form():
    if(request.method=='POST'):
        id_cliente=request.form.get("id_cli_edit")
        return render_template("edit_cliente.html",cliente=Clientes.get_idcliente(id_cliente))

@app.route("/edit_cliente",methods=['GET','POST'])
def edit_cliente():
    if(request.method=='POST'):
        idcliente=request.form.get("idcliente")
        nitcliente=request.form.get("nitcliente")
        nombres=request.form.get("nombres")
        apellidos=request.form.get("apellidos")
        telefono=request.form.get("telefono")
        email=request.form.get("email")
        fecharegistro=request.form.get("fecharegistro")
        sexo=request.form.get("sexo")
        activo=request.form.get("activo")
        Clientes.update_cliente(idcliente,nitcliente,nombres, apellidos, telefono, email, fecharegistro, sexo, activo)
        response= make_response(redirect('/get_clientes'))
        return response
    if(request.method=='GET'):
        print("El metodo es GET en edit_cliente")

@app.route("/form_add_cliente",methods=['GET','POST'])
def form_add_cliente():
    return render_template('add_cliente.html')

@app.route("/add_cliente",methods=['GET','POST'])
def add_cliente():
    # if(request.method=='POST'):
    #     nitcliente=request.form.get("nitcliente")
    #     nombres=request.form.get("nombres")
    #     apellidos=request.form.get("apellidos")
    #     telefono=request.form.get("telefono")
    #     email=request.form.get("email")
    #     fecharegistro=request.form.get("fecharegistro")
    #     sexo=request.form.get("sexo")
    #     activo=request.form.get("activo")
    #     cliente_to_add = Clientes(nitcliente,nombres, apellidos, telefono, email, fecharegistro, sexo, activo)
    #     cliente_to_add.create()
        routesClientes.add_cliente()
    
        response= make_response(redirect('/get_clientes'))
        #response= make_response(redirect('/'))
        return response
    
if __name__=="__main__":
    #punto de partida de ejecucion del programa    
    print("arrancando servidor...")
    app.run(debug=True,host="0.0.0.0")